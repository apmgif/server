FROM mhart/alpine-node:latest

# Create the app directory and set as working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy local files to container's working directory
COPY . /usr/src/app

# Install node packages
RUN npm install --quiet

# Build the application
RUN npm run build

# Expose port 4000 from the container to the host
EXPOSE 4000

CMD ["npm", "run", "serve"]
