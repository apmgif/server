const rewriteFilePath = (file, pathUrl) => {
  return {
    ...file,
    path: file.path.replace('./', pathUrl)
  }
}

export const rewriteFilesPath = (files, pathUrl) => {
  if (!files) return null

  if (files instanceof Array) {
    return files.map(file => rewriteFilePath(file, pathUrl))
  }
  return rewriteFilePath(files, pathUrl)
}
