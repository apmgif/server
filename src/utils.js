import { URLSearchParams } from 'url'

export const tagReducer = (giftags, tags) => {
  return tags.reduce((totalCount, tag) => {
    return totalCount + giftags.reduce((count, giftag) => tag === giftag ? ++count : count, 0)
  }, 0)
}

// sort descending (most matches >> least matches)
export const matchSorter = matches => {
  return matches.sort((match1, match2) => match2.hits - match1.hits)
}

// count number of tag matches for every item
export const hitCounter = (data, tags) => {
  return data.map(item => {
    const hits = tagReducer(item.tags, tags)
    return {
      ...item,
      hits
    }
  })
}

export const tagString2Array = tagString => {
  return tagString.trim().split(',')
}

export const jsonToFormParams = data => {
  const params = new URLSearchParams()
  Object.keys(data).map(key => params.append(key, data[key]))
  return params
}
