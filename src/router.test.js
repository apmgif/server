import express from 'express'
import request from 'supertest'
import bodyParser from 'body-parser'

jest.mock('./dbconnection', () => Promise.resolve({}))
jest.mock('./gifs-service', () => ({
  getApmGifList: () => ({ id: 'getApmGifList' }),
  findApmGifsByTags: tags => ({ id: tags }),
  getApmGifById: id => ({ id }),
  getApmGifByTags: tags => ({ id: tags }),
  getRandomApmGif: val => ({ id: val }),
  updateApmGif: json => ({ updated: json['name'] }),
  batchUpdateApmGifs: json => ({ updated: json['name'] }),
  deleteApmGif: id => ({ deleted: id })
}))
jest.mock('./result-parser', () => ({
  rewriteFilesPath: val => val
}))
jest.mock('./config')

import { isReadOnly } from './config'

describe('Router', () => {
  let app
  beforeEach(() => {
    app = express()
    app.use(bodyParser.json())
    app.use(require('./router').default)
  })
  describe('GET /', () => {
    test('returns 200', done => {
      request(app)
        .get('/')
        .set('Accept', 'application/json')
        .expect(200, done)
    })
  })

  describe('GET /gifs', () => {
    test('responds with json', done => {
      request(app)
        .get('/gifs')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { id: 'getApmGifList' }, done)
    })
  })

  describe('GET /gifs/:tags', () => {
    test('responds with json', done => {
      const tags = 'sa,matao,paco'
      request(app)
        .get(`/gifs/${tags}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { id: tags.split(',') }, done)
    })
  })

  describe('GET /gif/:tags', () => {
    test('responds with json', done => {
      const tags = 'sa,matao,paco'
      request(app)
        .get(`/gif/${tags}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { id: tags.split(',') }, done)
    })
  })

  describe('GET /gif/id/:id', () => {
    test('responds with json', done => {
      request(app)
        .get(`/gif/id/12345`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { id: '12345' }, done)
    })
  })

  describe('GET /gif/random/:tagged', () => {
    test('responds with json', done => {
      request(app)
        .get(`/gif/random/1`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { id: true }, done)
    })
  })

  describe('POST /gif/id/:id', () => {
    test('responds with unauthorized when readonly mode is activated', done => {
      isReadOnly.mockImplementationOnce(() => true)
      request(app)
        .post(`/gif/id/12345`)
        .send({ name: 'samataopaco.jpg' })
        .set('Accept', 'application/json')
        .expect(401, done)
    })

    test('responds with json', done => {
      isReadOnly.mockImplementationOnce(() => false)
      request(app)
        .post(`/gif/id/12345`)
        .send({ name: 'samataopaco.jpg' })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { updated: 'samataopaco.jpg' }, done)
    })
  })

  describe('POST /gifs', () => {
    test('responds with unauthorized when readonly mode is activated', done => {
      isReadOnly.mockImplementationOnce(() => true)
      request(app)
        .post(`/gif/id/12345`)
        .send({ name: 'samataopaco.jpg' })
        .set('Accept', 'application/json')
        .expect(401, done)
    })

    test('responds with json', done => {
      isReadOnly.mockImplementationOnce(() => false)
      request(app)
        .post(`/gif/id/12345`)
        .send({ name: 'samataopaco.jpg' })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { updated: 'samataopaco.jpg' }, done)
    })
  })

  describe('DELETE /gif/id/:id', () => {
    test('responds with unauthorized when readonly mode is activated', done => {
      isReadOnly.mockImplementationOnce(() => true)
      request(app)
        .delete(`/gif/id/12345`)
        .set('Accept', 'application/json')
        .expect(401, done)
    })

    test('responds with json', done => {
      isReadOnly.mockImplementationOnce(() => false)
      request(app)
        .delete(`/gif/id/12345`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, { deleted: '12345' }, done)
    })
  })
})
