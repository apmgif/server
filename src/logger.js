import fs from 'fs'
import { LogLevels, LOG_LEVEL_VALUE } from './config'

if (!fs.existsSync('logs')) {
  fs.mkdirSync('logs')
}

const infoStream = fs.createWriteStream('logs/info.txt')
const errorStream = fs.createWriteStream('logs/error.txt')

const log = (stream, message, level) => {
  if (level.value <= LOG_LEVEL_VALUE) {
    stream.write(`[${level.name}] ${new Date().toISOString()}: ${message}\n`)
  }
}

const info = (message) => {
  log(infoStream, message, LogLevels.info)
}

const error = (message) => {
  log(errorStream, message, LogLevels.error)
}

export default {
  info,
  error
}
