import { MongoClient } from 'mongodb'
import { MONGO_URL } from './config'

const dbConnection = MongoClient.connect(MONGO_URL)

export default dbConnection
