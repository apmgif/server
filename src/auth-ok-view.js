export default teamName => {
  return `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <title>APM?Gif</title>
        <meta charset="utf-8" />
        <style>
          body {
            font-family: sans-serif;
            padding: 0 10px;
          }
        </style>
      </head>
      <body>
          <h1>Gràcies! 🙌</h1>
          <p><strong>APM?Gif</strong> s'ha instal·lat correctament a l'Slack team <i>${teamName}</i> 👍</p>
          <img alt="japos aplaudint" title="japos aplaudint" src="https://images.apmgif.cat/images/tumblr_m5u5hnHmrR1rz12m1o1_250.gif" />
      </body>
    </html>
  `
}
