import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import dbConnection from './dbconnection'
import router from './router'
import {
  PORT,
  MONGO_URL,
  ENVIRONMENT,
  LOG_LEVEL_NAME
} from './config'

const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/images', express.static('./images'))
app.use(router)

/**
 * start listening to connections
 */
app.listen(PORT, async () => {
  console.log('' +
    `   API started: Listening on port ${PORT}.\n` +
    `   Environment: ${ENVIRONMENT}.\n` +
    `   MongoDB: ${MONGO_URL}.\n` +
    `   Log Level: ${LOG_LEVEL_NAME}.\n`)

  try {
    await dbConnection
    console.log('   DB connection established\n')
  } catch (err) {
    console.error(err)
  }
})
