export const composeSlackResponse = (file, text = '') => {
  return {
    response_type: 'in_channel',
    delete_original: true,
    attachments: [{
      image_url: file.path,
      text
    }]
  }
}

export const composeSlackInteractiveMessage = (files, text = '') => {
  const attachments = [
    ...files.map(file => createAttachment(file, text)),
    createCancelButton()
  ]

  return {
    response_type: 'ephemeral',
    text: `He trobat les següents imatges. Quina buscaves?`,
    attachments
  }
}

const createAttachment = (file, name) => {
  return {
    image_url: file.path,
    text: file.tags.join(' '),
    callback_id: file['_id'],
    actions: [{
      name,
      text: 'Aquesta!',
      type: 'button',
      value: file['_id']
    }]
  }
}

const createCancelButton = (tags = []) => {
  return {
    text: 'O prefereixes avortar la missió?',
    callback_id: 'cancel',
    actions: [{
      name: 'cancel',
      text: 'Avortar',
      type: 'button',
      value: 'cancel',
      style: 'danger'
    }]
  }
}
