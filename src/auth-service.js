import dbConnection from './dbconnection'

const collectionName = 'authorizedTeams'

/**
 * Store a Slack team's access token in the db, along with other team info
 * @param {object} teamInfo
 */
export const storeTeamAccessToken = async teamInfo => {
  try {
    const db = await dbConnection
    return db.collection(collectionName).insert(teamInfo)
  } catch (err) {
    throw err
  }
}

export const fetchAuthorizedTeamNames = async () => {
  try {
    const db = await dbConnection
    const data = await db.collection(collectionName).find({}).toArray()
    return data.map(item => item.teamName)
  } catch (err) {
    throw err
  }
}

/**
 * Fetches a Slack team entry by the team id
 * @param {string} teamId: Slack team id
 */
export const fetchTeamById = async teamId => {
  try {
    const db = await dbConnection
    return db.collection(collectionName).find({ teamId })
  } catch (err) {
    throw err
  }
}

/**
 * Fetches a Slack team's access token by the team id
 * @param {string} teamId: Slack team id
 */
export const fetchTeamToken = async teamId => {
  try {
    const data = await fetchTeamById(teamId)
    return data.accessToken
  } catch (err) {
    throw err
  }
}
