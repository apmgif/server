import dotenv from 'dotenv'

dotenv.config()

export const LogLevels = {
  none: { name: 'NONE', value: 0 },
  info: { name: 'INFO', value: 1 },
  error: { name: 'ERROR', value: 2 }
}

const MODE_READONLY = 'readonly'
const MODE_READWRITE = 'readwrite'

export const PORT = process.env.PORT || 4000
export const ENVIRONMENT = process.env.NODE_ENV
export const IMAGES_URL = process.env.IMAGES_URL
export const MONGO_URL = process.env.MONGO_URL
export const SLACK_CLIENT_ID = process.env.SLACK_CLIENT_ID
export const SLACK_CLIENT_SECRET = process.env.SLACK_CLIENT_SECRET
export const SLACK_AUTH_REDIRECT_URI = process.env.SLACK_AUTH_REDIRECT_URI
export const SLACK_VERIFICATION_TOKEN = process.env.SLACK_VERIFICATION_TOKEN
export const LOG_LEVEL_VALUE = process.env.LOG_LEVEL ? LogLevels[process.env.LOG_LEVEL].value : LogLevels.none.value
export const LOG_LEVEL_NAME = process.env.LOG_LEVEL ? LogLevels[process.env.LOG_LEVEL].name : LogLevels.none.name
export const DB_MODE = process.env.DB_MODE || MODE_READONLY

export const HTTP_OK = 200
export const HTTP_BAD_REQUEST = 400
export const HTTP_UNAUTHORIZED = 401

export const SLACK_OAUTH_ACCESS_URL = 'https://slack.com/api/oauth.access'

export const isDevelopment = () => {
  return ENVIRONMENT === 'development'
}

export const isTokenValid = token => {
  return SLACK_VERIFICATION_TOKEN === token
}

export const isReadOnly = () => {
  return DB_MODE === MODE_READONLY
}
