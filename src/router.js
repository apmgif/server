import fetch from 'node-fetch'
import express from 'express'

import dbConnection from './dbconnection'
import {
  isDevelopment,
  isReadOnly,
  isTokenValid,
  HTTP_OK,
  HTTP_BAD_REQUEST,
  HTTP_UNAUTHORIZED,
  IMAGES_URL,
  SLACK_CLIENT_ID,
  SLACK_CLIENT_SECRET,
  SLACK_AUTH_REDIRECT_URI,
  SLACK_OAUTH_ACCESS_URL
} from './config'
import {
  getApmGifList,
  getApmGifById,
  getApmGifByTags,
  getRandomApmGif,
  findApmGifsByTags,
  batchUpdateApmGifs,
  updateApmGif,
  deleteApmGif
} from './gifs-service'
import {
  storeTeamAccessToken
} from './auth-service'
import authOkView from './auth-ok-view'
import {
  composeSlackResponse,
  composeSlackInteractiveMessage
} from './message-composer'
import {
  rewriteFilesPath
} from './result-parser'
import logger from './logger'
import {
  tagString2Array,
  jsonToFormParams
} from './utils'

const router = express.Router()

export default router

/**
 * Healthcheck
 * Verb: GET
 */
router.get('/', async (_, res) => {
  try {
    await dbConnection
    res.status(HTTP_OK)
    res.send('Alguna pregunta més?')
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Returns APM? gif list
 * Verb: GET
 */
router.get('/gifs', async (_, res) => {
  try {
    const data = await getApmGifList()
    respondOk(res, rewriteFilesPath(data, getImagesUrl()))
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Returns a list of APM? gifs which match the tags provided
 * Tags should be provided as comma separated words in a string
 * Verb: GET
 */
router.get('/gifs/:tags', async (req, res) => {
  try {
    const tags = tagString2Array(req.params.tags)
    const data = await findApmGifsByTags(tags)
    respondOk(res, rewriteFilesPath(data, getImagesUrl()))
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Returns APM? gif by its unique ID
 * Verb: GET
 */
router.get('/gif/id/:id', async (req, res) => {
  try {
    const data = await getApmGifById(req.params.id)
    respondOk(res, rewriteFilesPath(data, getImagesUrl()))
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Returns a random tagged or untagged APM? gif
 * Verb: GET
 */
router.get('/gif/random/:tagged', async (req, res) => {
  try {
    const tagged = parseInt(req.params.tagged, 10)
    const data = await getRandomApmGif(!!tagged)
    respondOk(res, rewriteFilesPath(data, getImagesUrl()))
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Returns the APM? gif that best matches the tags provided
 * Tags should be provided as comma separated words in a string
 * Verb: GET
 */
router.get('/gif/:tags', async (req, res) => {
  try {
    const tags = tagString2Array(req.params.tags)
    const data = await getApmGifByTags(tags)
    respondOk(res, rewriteFilesPath(data, getImagesUrl()))
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Updates a gif
 * Verb: POST
 */
router.post('/gif/id/:id', async (req, res) => {
  try {
    if (isReadOnly()) {
      return respondUnauthorized(res)
    }
    const data = await updateApmGif(req.body)
    respondOk(res, data)
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Updates a collection of gifs
 * Verb: POST
 */
router.post('/gifs', async (req, res) => {
  try {
    if (isReadOnly()) {
      return respondUnauthorized(res)
    }
    const data = await batchUpdateApmGifs(req.body)
    respondOk(res, data)
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * Deletes a gif
 * Verb: DELETE
 */
router.delete('/gif/id/:id', async (req, res) => {
  try {
    if (isReadOnly()) {
      return respondUnauthorized(res)
    }
    const data = await deleteApmGif(req.params.id)
    respondOk(res, data)
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/*******************************
 * SLACK COMMAND ENDPOINTS
 */

/**
 * This is the endpoint that the Slack command /apm will POST to
 * The payload to this request has the following signature
 * {
 *   "token": "JDY32wV5GJVJ9fpUQefejZeZ"
 *   "team_id": "T0001"
 *   "team_domain": "example"
 *   "channel_id": "C2147483705"
 *   "channel_name": "test"
 *   "user_id": "U2147483697"
 *   "user_name": "Silvia"
 *   "command": "/apm"
 *   "text": "sa matao paco"
 *   "response_url": "https://hooks.slack.com/commands/1234/5678"
 * }
 * Verb: POST
 */
router.post('/gif', async (req, res) => {
  const {
    text,
    token
  } = req.body

  if (!isDevelopment() && !isTokenValid(token)) {
    res.status(HTTP_UNAUTHORIZED)
    res.send('Ep! No estàs autoritzat a utilitzar aquesta comanda')
    return
  }

  try {
    const tags = text.split(' ')
    logger.info(`[IN] POST /gif [tags: ${tags}]`)

    const data = await getApmGifByTags(tags)
    let response = {}
    if (!data) {
      response = {
        text: `Vaja! No he trobat gifs que tinguin els tags proporcionats: '${text}' :(`
      }
    } else {
      response = composeSlackResponse(rewriteFilesPath(data, getImagesUrl()))
    }
    respondOk(res, response)
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * This is the endpoint that the Slack command /apm? will POST to
 * The payload to this request has the following signature
 * {
 *   "token": "JDY32wV5GJVJ9fpUQefejZeZ"
 *   "team_id": "T0001"
 *   "team_domain": "example"
 *   "channel_id": "C0123456789"
 *   "channel_name": "test"
 *   "user_id": "U0123456789"
 *   "user_name": "Silvia"
 *   "command": "/apm"
 *   "text": "sa matao paco"
 *   "response_url": "https://hooks.slack.com/commands/1234/5678"
 * }
 * Verb: POST
 */
router.post('/gif/s', async (req, res) => {
  const {
    text,
    token
  } = req.body

  if (!isDevelopment() && !isTokenValid(token)) {
    res.status(HTTP_UNAUTHORIZED)
    res.send('Ep! No estàs autoritzat a utilitzar aquesta comanda')
    return
  }

  try {
    const tags = text.split(' ')
    const data = await findApmGifsByTags(tags)
    const response = !data.length
      ? { text: `Vaja! No he trobat gifs que tinguin els tags proporcionats: '${text}' :(` }
      : composeSlackInteractiveMessage(rewriteFilesPath(data.slice(0, 10), getImagesUrl()), text)

    respondOk(res, response)
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/**
 * This is the endpoint that Slack's interactive message actions will POST to
 * The payload to this request has the following signature
 * {
 *  "actions": [
 *    {
 *      "name": "el_rio_una_mierda",
 *      "value": "7458c3t8c3468c56",
 *      "type": "button"
 *    }
 *  ],
 *  "callback_id": "el_rio_una_mierda",
 *  "team": {
 *    "id": "T12345678",
 *    "domain": "typeform"
 *  },
 *  "channel": {
 *    "id": "C12345678",
 *    "name": "stickbug"
 *  },
 *  "user": {
 *    "id": "U12345678",
 *    "name": "pchiwan"
 *  },
 *  "action_ts": "1458170917.164398",
 *  "message_ts": "1458170866.000004",
 *  "attachment_id": "1",
 *  "token": "JDY32wV5GJVJ9fpUQefejZeZ",
 *  "response_url": "https://hooks.slack.com/actions/T12345678/6204672533/x7ZLaiVMoECAW50Gw1ZYAXEM"
 * }
 * Verb: POST
 */
router.post('/action', async (req, res) => {
  const {
    actions,
    callback_id: callbackId,
    response_url: responseUrl,
    token
  } = JSON.parse(req.body.payload)

  if (!isDevelopment() && !isTokenValid(token)) {
    res.status(HTTP_UNAUTHORIZED)
    res.send('Ep! No estàs autoritzat a utilitzar aquesta comanda')
    return
  }

  logger.info(`[IN] POST /action [actions: ${JSON.stringify(actions)}, response_url: ${responseUrl}]`)

  // reply right away to make sure the request doesn't time out
  // and keep interacting with Slack through the response URL
  respondOk(res, {
    delete_original: true,
    response_type: 'ephemeral'
  })

  if (callbackId !== 'cancel') {
    try {
      const [{ value, name }] = actions
      const data = await getApmGifById(value)
      const body = JSON.stringify(composeSlackResponse(rewriteFilesPath(data, getImagesUrl()), name))
      wrappedFetch(responseUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body
      })
    } catch (err) {
      logger.error(err)
    }
  }
})

/*******************************
 * SLACK AUTHORIZATION FLOW
 */

 /**
 * Redirect uri for the OAuth token authorization flow
 * Verb: GET
 *
 * {
 *   "access_token": "xoxp-XXXXXXXX-XXXXXXXX-XXXXX",
 *   "scope": "incoming-webhook,commands,bot",
 *   "team_name": "Team Installing Your Hook",
 *   "team_id": "T12345678",
 *   "incoming_webhook": {
 *       "url": "https://hooks.slack.com/TXXXXX/BXXXXX/XXXXXXXXXX",
 *       "channel": "#channel-it-will-post-to",
 *       "configuration_url": "https://teamname.slack.com/services/BXXXXX"
 *   },
 *   "bot":{
 *       "bot_user_id":"UTTTTTTTTTTR",
 *       "bot_access_token":"xoxb-XXXXXXXXXXXX-TTTTTTTTTTTTTT"
 *   }
* }
 */
router.get('/auth/redirect', async (req, res) => {
  const params = jsonToFormParams({
    code: req.query.code,
    client_id: SLACK_CLIENT_ID,
    client_secret: SLACK_CLIENT_SECRET,
    redirect_uri: SLACK_AUTH_REDIRECT_URI
  })

  try {
    const response = await wrappedFetch(SLACK_OAUTH_ACCESS_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: params
    })
    const data = await response.json()

    logger.info(`Response from ${SLACK_OAUTH_ACCESS_URL}: ${JSON.stringify(data)}`)
    if (data.ok) {
      const {
        team_id: teamId,
        team_name: teamName,
        access_token: accessToken,
        user_id: userId
      } = data

      await storeTeamAccessToken({
        teamId: teamId,
        teamName: teamName,
        accessToken: accessToken,
        userId: userId
      })
      res.status(HTTP_OK)
      res.send(authOkView(teamName))
    }
  } catch (err) {
    respondBadRequest(res, err)
  }
})

/*******************************
 * TESTING PURPOSES
 */

/**
 * I'll use this endpoint to locally test the action response workflow
 * so that instead of posting to the response_url provided by Slack I'll post here
 * Verb: POST
 */
router.post('/response', (req, res) => {
  logger.info(`[IN] POST /response [text: ${req.body.text}]`)
  res.status(HTTP_OK)
  res.send()
})

/*******************************
 * PRIVATE METHODS
 */

const wrappedFetch = (url, options) => {
  logger.info(`[OUT] URL: ${url}  [options: ${JSON.stringify(options)}]`)
  return fetch(url, options)
}

const getImagesUrl = () => {
  return `${IMAGES_URL}/`
}

const respondOk = (res, data) => {
  logger.info(`HTTP_OK [data: ${JSON.stringify(data)}]`)
  res.status(HTTP_OK)
  res.json(data)
}

const respondBadRequest = (res, err) => {
  logger.error(`HTTP_BAD_REQUEST [error: ${err}]`)
  res.status(HTTP_BAD_REQUEST)
  res.send(err)
}

const respondUnauthorized = res => {
  res.status(HTTP_UNAUTHORIZED)
  res.send(`Mode lectura activat: no es poden realitzar operacions d'escriptura`)
}
