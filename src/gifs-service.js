import { ObjectID } from 'mongodb'
import dbConnection from './dbconnection'
import {
  hitCounter,
  matchSorter
} from './utils'

const collectionName = 'gifs'

/**
 * Get list of all gif entries in database
 */
export const getApmGifList = async () => {
  try {
    const db = await dbConnection
    return db.collection(collectionName).find({}).toArray()
  } catch (err) {
    throw err
  }
}

/**
 * Get a gif entry by its id
 * @param {string} id: the gif entry's ObjectID
 */
export const getApmGifById = async id => {
  try {
    const db = await dbConnection
    return db.collection(collectionName).findOne(new ObjectID(id))
  } catch (err) {
    throw err
  }
}

/**
 * Get list of gif entries with matching tags, sorted descendingly by number of matches
 * @param {string[]} tags: list of tags to match
 */
export const findApmGifsByTags = async tags => {
  try {
    const db = await dbConnection
    const data = await db.collection(collectionName).find({ tags: { $in: tags } }).toArray()
    return matchSorter(hitCounter(data, tags), tags)
  } catch (err) {
    throw err
  }
}

/**
 * Get first gif entry with the most matching tags
 * @param {string[]} tags: list of tags to match
 */
export const getApmGifByTags = async tags => {
  try {
    const data = await findApmGifsByTags(tags)
    return data.length ? data[0] : null
  } catch (err) {
    throw err
  }
}

/**
 * Get a random gif entry from the database
 * @param {boolean} tagged: indicates whether you want a tagged or untagged random gif
 */
export const getRandomApmGif = async (tagged = false) => {
  try {
    const db = await dbConnection
    const where = tagged ? { $ne: [] } : { $eq: [] }
    // fetch a single random item (tagged or untagged)
    return new Promise((resolve, reject) => {
      db.collection(collectionName).aggregate([
        { $match: { tags: where } },
        { $sample: { size: 1 } }
      ], (err, result) => {
        return err ? reject(err) : resolve(result)
      })
    })
  } catch (err) {
    throw err
  }
}

/**
 * Update a list of gif entries
 * @param {object[]} gifs
 */
export const batchUpdateApmGifs = async gifs => {
  try {
    const db = await dbConnection
    const data = await Promise.all(gifs.map(gif => {
      return db.collection(collectionName).update({
        _id: new ObjectID(gif['_id'])
      }, {
        $set: {
          name: gif.name,
          tags: gif.tags
        }
      })
    }))
    const result = data.reduce((count, res) => {
      return res && res.result ? count + res.result.nModified : count
    }, 0)
    return `${result} modified documents`
  } catch (err) {
    throw err
  }
}

/**
 * Update a single gif entry
 * @param {object} gif
 */
export const updateApmGif = gif => {
  return batchUpdateApmGifs([gif])
}

/**
 * Delete a gif entry from the database
 * @param {string} id: the gif entry's ObjectID
 */
export const deleteApmGif = async id => {
  try {
    const db = await dbConnection
    return db.collection(collectionName).remove({
      _id: new ObjectID(id)
    })
  } catch (err) {
    throw err
  }
}
