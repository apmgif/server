APM?Gif API
===========

This is the Express API to tag and retrieve GIFs, which also exposes the necessary endpoints for the Slack integration.


## Endpoints

### GET `/`
Use this methods to check whether the API is up and listening

**Example**
```
GET `/`
```

### GET `/gifs`
Returns a list of all APM? gifs in the database

**Example**
```
GET /gifs
```

### GET `/gifs/:tags`
Returns a list of APM? gifs which match at least one of the tags provided. Tags should be provided as comma separated words in a string, e.g.: `bendiciones,buenas,noches`

**Example**
```
GET /gifs/rio,mierda
GET /gifs/bendiciones,buenas,noches
```

### GET `/gif/id/:id`
Returns an APM? gif by its unique ID

**Example**
```
GET /gif/id/590db7aa7159c2aae6e825a2
GET /gif/id/590db7aa7159c2aae6e825af
```

### GET `/gif/:tags`
Returns the APM? gif that best matches the tags provided. Tags should be provided as comma separated words in a string, e.g.: `rio,mierda`

**Example**
```
GET /gif/rio,mierda
GET /gif/bendiciones,buenas,noches
```

### GET `/gif/random/:tagged`
Returns a random APM? gif. You must specify whether you want the gif to be tagged (0) or not (1).

**Example**
```
GET /gif/random/0
GET /gif/random/1
```

### POST `/gif/id/:id`
Updates a single gif, specified by its unique ID. The payload for this request must be a JSON object with the following interface:
```
{
  _id: String,
  name: String,
  path: String,
  tags: String[]
}
```

**Example**
```
POST /id/590db7aa7159c2aae6e825a2
Content-Type: application/json
Payload:
{
  "_id" : "590db7aa7159c2aae6e825a2",
  "name" : "por_dios_chiste_mlao",
  "path" : "./images/tumblr_inline_hjfgy878yigdfhjkas.gif",
  "tags" : [ "por", "dios", "chiste", "malo" ]
}
```

### POST `/gifs`
Updates a collection of gifs. The payload for this request must be an array of JSON objects with the following interface:
```
{
  _id: String,
  name: String,
  path: String,
  tags: String[]
}
```

**Example**
```
POST /gifs
Content-Type: application/json
Payload:
[
  {
    "_id" : "590db7aa7159c2aae6e825a8",
    "name" : "chistes_nivel",
    "path" : "./images/tumblr_inline_mfwquhlgBi1qecxse.gif",
    "tags" : [ "chistes", "nivel" ]
  },
  {
    "_id" : "590db7aa7159c2aae6e825af",
    "name" : "gente_muy_rara",
    "path" : "./images/tumblr_inline_mfwr3yxu4k1qecxse.gif",
    "tags" : [ "gente", "muy", "rara" ]
  }
]
```


### DELETE `/gif/id/:id`
Deletes a single gif, specified by its unique ID.

**Example**
```
DELETE /gif/id/590db7aa7159c2aae6e825a2
```
